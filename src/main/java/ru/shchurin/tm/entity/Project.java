package ru.shchurin.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.util.DateUtil;

import java.util.Date;
@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractEntity {

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    private Date startDate;

    @Nullable
    private Date endDate;

    @Nullable
    private String userId;

    public Project(@Nullable String id, @Nullable String name, @Nullable Date startDate, @Nullable Date endDate,
                   @Nullable String userId) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.userId = userId;
    }

    public Project(@Nullable String name, @Nullable Date startDate, @Nullable Date endDate, @Nullable String userId) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "PROJECT{" +
                "ID:'" + id + '\'' +
                ", PROJECT NAME:'" + name + '\'' +
//                ", description='" + description + '\'' +
                ", START_DATE:" + DateUtil.dateFormat(startDate) +
                ", END_DATE:" + DateUtil.dateFormat(endDate) +
                '}';
    }
}
