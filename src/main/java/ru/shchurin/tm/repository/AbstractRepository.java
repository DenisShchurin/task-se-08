package ru.shchurin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.entity.AbstractEntity;
import ru.shchurin.tm.exception.AlreadyExistsException;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractRepository<T extends AbstractEntity> {
    @NotNull
    protected final Map<String, T> entities = new HashMap<>();

    public void persist(@NotNull final T entity) throws AlreadyExistsException {
        if (entities.containsKey(entity.getId()))
            throw new AlreadyExistsException();
        entities.put(entity.getId(), (T) entity);
    }

    public void merge(@NotNull final T entity) {
        entities.put(entity.getId(), (T) entity);
    }

}
