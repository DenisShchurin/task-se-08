package ru.shchurin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.api.UserRepository;
import ru.shchurin.tm.entity.User;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

public final class UserRepositoryImpl extends AbstractRepository<User> implements UserRepository {
    @NotNull
    @Override
    public Collection<User> findAll() {
        return entities.values();
    }

    @Nullable
    @Override
    public User findOne(@NotNull final String id) {
        @Nullable final User user = entities.get(id);
        if (user == null) return null;
        return user;
    }

    @Override
    public void remove(@NotNull final String id) {
        entities.remove(id);
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        for (Iterator<Map.Entry<String, User>> it = entities.entrySet().iterator(); it.hasNext();) {
            @NotNull final Map.Entry<String, User> e = it.next();
            if (login.equals(e.getValue().getLogin())) {
                it.remove();
            }
        }
    }

    @Override
    public boolean updatePassword(@NotNull final String login, @NotNull final String hashPassword,
                                  @NotNull final String newHashPassword) {
        for (@NotNull final User user : entities.values()) {
            if (user.getLogin().equals(login) && user.getHashPassword().equals(hashPassword)) {
                user.setHashPassword(newHashPassword);
                return true;
            }
        }
        return false;
    }
}
