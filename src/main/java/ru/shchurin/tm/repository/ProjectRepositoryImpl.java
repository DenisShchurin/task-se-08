package ru.shchurin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.api.ProjectRepository;
import ru.shchurin.tm.entity.Project;

import java.util.*;

public final class ProjectRepositoryImpl extends AbstractRepository<Project> implements ProjectRepository {
    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        @NotNull final List<Project> userProjects = new ArrayList<>();
        for (@NotNull final Project project : entities.values()) {
            if (userId.equals(project.getUserId()))
                userProjects.add(project);
        }
        return userProjects;
    }

    @Nullable
    @Override
    public Project findOne(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = entities.get(id);
        if (project == null) return null;
        if (project.getUserId().equals(userId)) return project;
        return null;
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = entities.get(id);
        if (project == null) return;
        if (project.getUserId().equals(userId))
            entities.remove(id);
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        for (Iterator<Map.Entry<String, Project>> it = entities.entrySet().iterator(); it.hasNext(); ) {
            @NotNull final Map.Entry<String, Project> e = it.next();
            if (userId.equals(e.getValue().getUserId()))
                it.remove();
        }
    }

    @Override
    public void removeByName(@NotNull final String userId, @NotNull final String name) {
        for (Iterator<Map.Entry<String, Project>> it = entities.entrySet().iterator(); it.hasNext(); ) {
            @NotNull final Map.Entry<String, Project> e = it.next();
            if (name.equals(e.getValue().getName()) && userId.equals(e.getValue().getUserId()))
                it.remove();
        }
    }
}
