package ru.shchurin.tm;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.bootstrap.Bootstrap;
import ru.shchurin.tm.command.*;

public final class Application {
    @NotNull
    private static final Class[] CLASSES = {
            HelpCommand.class, ExitCommand.class,
            ProjectClearCommand.class, ProjectCreateCommand.class,
            ProjectUpdateCommand.class, ProjectListCommand.class,
            ProjectRemoveCommand.class, TaskClearCommand.class,
            TaskCreateCommand.class, TaskUpdateCommand.class,
            TaskListCommand.class, TaskRemoveCommand.class,
            TasksOfProjectCommand.class, UserAuthorizationCommand.class,
            UserEditProfileCommand.class, UserEndSessionCommand.class,
            UserRegistrationCommand.class, UserShowProfileCommand.class,
            UserUpdatePasswordCommand.class, UserListCommand.class,
            AboutCommand.class};

    public static void main(String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(CLASSES);
        bootstrap.initUsers();
        bootstrap.start();
    }
}
