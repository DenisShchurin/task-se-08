package ru.shchurin.tm.exception;

public class ConsoleIdException extends Exception {
    public ConsoleIdException() {
        super("YOU ENTERED INCORRECT ID");
    }
}
