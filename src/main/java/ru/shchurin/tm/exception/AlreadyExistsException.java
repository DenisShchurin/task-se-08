package ru.shchurin.tm.exception;

public class AlreadyExistsException extends Exception{
    public AlreadyExistsException() {
        super("THIS ENTITY ALREADY EXISTS");
    }
}
