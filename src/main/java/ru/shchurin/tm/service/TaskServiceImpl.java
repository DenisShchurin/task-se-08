package ru.shchurin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.api.TaskService;
import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.api.TaskRepository;
import ru.shchurin.tm.exception.*;
import java.util.List;

public final class TaskServiceImpl implements TaskService {
    private final TaskRepository taskRepository;

    public TaskServiceImpl(@NotNull TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId) throws UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        return taskRepository.findAll(userId);
    }

    @NotNull
    @Override
    public Task findOne(@Nullable final String userId, @Nullable final String id) throws ConsoleIdException,
            TaskNotFoundException, UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        if (id == null || id.isEmpty())
            throw new ConsoleIdException();
        @Nullable
        final Task task = taskRepository.findOne(userId, id);
        if (task == null)
            throw new TaskNotFoundException();
        return task;
    }

    @Override
    public void persist(@Nullable final Task task) throws Exception {
        if (task == null)
            return;
        if (task.getName() == null || task.getName().isEmpty())
            throw new ConsoleNameException();
        if (task.getStartDate() == null)
            throw new ConsoleStartDateException();
        if (task.getEndDate() == null)
            throw new ConsoleEndDateException();
        taskRepository.persist(task);
    }

    @Override
    public void merge(@Nullable final Task task) throws ConsoleNameException, ConsoleStartDateException,
            ConsoleEndDateException {
        if (task == null)
            return;
        if (task.getName() == null || task.getName().isEmpty())
            throw new ConsoleNameException();
        if (task.getStartDate() == null)
            throw new ConsoleStartDateException();
        if (task.getEndDate() == null)
            throw new ConsoleEndDateException();
        taskRepository.merge(task);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) throws ConsoleIdException,
            UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        if (id == null || id.isEmpty())
            throw new ConsoleIdException();
        taskRepository.remove(userId, id);
    }

    @Override
    public void removeAll (@Nullable final String userId) throws UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        taskRepository.removeAll(userId);
    }

    @Override
    public void removeByName(@Nullable final String userId, @Nullable final String name) throws ConsoleNameException,
            UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        if (name == null || name.isEmpty())
            throw new ConsoleNameException();
        taskRepository.removeByName(userId, name);
    }
}
