package ru.shchurin.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.entity.AbstractEntity;

public interface Repository <T extends AbstractEntity> {
    void persist(@NotNull T entity) throws Exception;

    void merge(@NotNull T entity);
}
