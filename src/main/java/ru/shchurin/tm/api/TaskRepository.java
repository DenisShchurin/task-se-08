package ru.shchurin.tm.api;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.entity.AbstractEntity;
import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.exception.AlreadyExistsException;

import java.util.List;

public interface TaskRepository extends Repository<Task> {
    @NotNull
    List<Task> findAll(@NotNull String userId);

    @Nullable
    Task findOne(@NotNull String userId, @NotNull String id);

    void remove(@NotNull String userId, @NotNull String id);

    void removeAll(@NotNull String userId);

    void removeByName(@NotNull String userId, @NotNull String name);
}
