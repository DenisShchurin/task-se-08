package ru.shchurin.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.entity.AbstractEntity;
import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.exception.AlreadyExistsException;

import java.util.List;

public interface ProjectRepository extends Repository<Project> {
    @NotNull
    List<Project> findAll(@NotNull String userId);

    @Nullable
    Project findOne(@NotNull String userId, @NotNull String id);

    void remove(@NotNull String userId, String id);

    void removeAll(@NotNull String userId);

    void removeByName(@NotNull String userId, @NotNull String name);
}
