package ru.shchurin.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.bootstrap.Bootstrap;
import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.util.ConsoleUtil;
import ru.shchurin.tm.util.DateUtil;
import ru.shchurin.tm.exception.*;

import java.text.ParseException;
import java.util.*;

public final class TaskUpdateCommand extends AbstractCommand {
    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return "task-update";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK UPDATE]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = ConsoleUtil.getStringFromConsole();
        System.out.println("ENTER ID:");
        @NotNull final String id = ConsoleUtil.getStringFromConsole();
        System.out.println("ENTER PROJECT_ID:");
        @NotNull final String projectId = ConsoleUtil.getStringFromConsole();
        System.out.println("ENTER START_DATE:");
        @NotNull final String start = ConsoleUtil.getStringFromConsole();
        @NotNull final Date startDate;
        try {
            startDate = DateUtil.parseDate(start);
        } catch (ParseException e) {
            System.out.println("YOU ENTERED WRONG START_DATE:");
            return;
        }
        System.out.println("ENTER END_DATE:");
        @NotNull final String end = ConsoleUtil.getStringFromConsole();
        @NotNull final Date endDate;
        try {
            endDate = DateUtil.parseDate(end);
        } catch (ParseException e) {
            System.out.println("YOU ENTERED WRONG END_DATE:");
            return;
        }
        @NotNull final Task task = new Task(id, name, projectId, startDate, endDate,
                ((Bootstrap)serviceLocator).getCurrentUser().getId());
        try {
            serviceLocator.getTaskService().merge(task);
            System.out.println("[PROJECT UPDATED]");
        } catch (ConsoleNameException | ConsoleStartDateException | ConsoleEndDateException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
