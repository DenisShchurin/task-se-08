package ru.shchurin.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.api.ServiceLocator;
import ru.shchurin.tm.entity.Role;

import java.util.List;

public abstract class AbstractCommand {
    protected ServiceLocator serviceLocator;

    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public abstract String getCommand();

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public abstract boolean isSafe();

    @NotNull
    public abstract List<Role> getRoles();
}
