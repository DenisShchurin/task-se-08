package ru.shchurin.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.bootstrap.Bootstrap;
import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.util.ConsoleUtil;

import java.util.ArrayList;
import java.util.List;

public final class UserUpdatePasswordCommand extends AbstractCommand {
    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>();

    @NotNull
    @Override
    public String getCommand() {
        return "user-update-password";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update user password";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final User currentUser = ((Bootstrap)serviceLocator).getCurrentUser();
        System.out.println("[USER PASSWORD UPDATE]");
        System.out.println("ENTER PASSWORD:");
        @NotNull final String newPassword = ConsoleUtil.getStringFromConsole();
        @NotNull final String newHashPassword = serviceLocator.getUserService().getHashOfPassword(newPassword);
        final boolean update = serviceLocator.getUserService().updatePassword(currentUser.getLogin(),
                currentUser.getHashPassword(), newHashPassword);
        if (update) {
            System.out.println("USER PASSWORD UPDATED");
        } else {
            System.out.println("USER PASSWORD IS NOT UPDATED");
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
