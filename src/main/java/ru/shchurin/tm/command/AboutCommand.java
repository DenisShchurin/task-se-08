package ru.shchurin.tm.command;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.entity.Role;

import java.util.ArrayList;
import java.util.List;

public class AboutCommand extends AbstractCommand{
    private final boolean safe = true;

    @NotNull
    private final List<Role> roles = new ArrayList<>();

    @NotNull
    @Override
    public String getCommand() {
        return "about";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Application assembly information.";
    }

    @Override
    public void execute() {
        System.out.println("Manifest-Version: " + Manifests.read("Manifest-Version"));
        System.out.println("Built-By: " + Manifests.read("Built-By"));
        System.out.println("Created-By: " + Manifests.read("Created-By"));
        System.out.println("Build-Jdk: " + Manifests.read("Build-Jdk"));
        System.out.println("Project-Version: " + Manifests.read("Project-Version"));
        System.out.println("Project-Name: " + Manifests.read("Project-Name"));
        System.out.println("Implementation-Build: " + Manifests.read("Implementation-Build"));
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
