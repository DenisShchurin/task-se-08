package ru.shchurin.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.bootstrap.Bootstrap;
import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.util.ConsoleUtil;

import java.util.ArrayList;
import java.util.List;

public final class UserAuthorizationCommand extends AbstractCommand {
    private final boolean safe = true;

    @NotNull
    private final List<Role> roles = new ArrayList<>();

    @NotNull
    @Override
    public String getCommand() {
        return "user-authorization";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Authorise user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER AUTHORIZATION]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = ConsoleUtil.getStringFromConsole();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = ConsoleUtil.getStringFromConsole();
        @NotNull final String hashOfPassword = serviceLocator.getUserService().getHashOfPassword(password);
        @NotNull final User currentUser = serviceLocator.getUserService().authoriseUser(login, hashOfPassword);
        if (currentUser == null) {
            System.out.println("[USER NOT FOUND]");
        } else {
            ((Bootstrap)serviceLocator).setCurrentUser(currentUser);
            System.out.println("[USER AUTHORIZED]");
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
