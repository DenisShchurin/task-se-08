package ru.shchurin.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.bootstrap.Bootstrap;
import ru.shchurin.tm.entity.Role;

import java.util.ArrayList;
import java.util.List;

public final class UserEndSessionCommand extends AbstractCommand {
    private final boolean safe = true;

    @NotNull
    private final List<Role> roles = new ArrayList<>();

    @NotNull
    @Override
    public String getCommand() {
        return "user-end-session";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "End user session";
    }

    @Override
    public void execute() {
        System.out.println("[USER END SESSION]");
        ((Bootstrap)serviceLocator).setCurrentUser(null);
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
