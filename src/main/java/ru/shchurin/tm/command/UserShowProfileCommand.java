package ru.shchurin.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.bootstrap.Bootstrap;
import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public final class UserShowProfileCommand extends AbstractCommand {
    private final boolean safe = false;

    @NotNull
    private final ArrayList<Role> roles = new ArrayList<>();

    @NotNull
    @Override
    public String getCommand() {
        return "user-show-profile";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show user profile";
    }

    @Override
    public void execute() {
        @NotNull final User currentUser = ((Bootstrap)serviceLocator).getCurrentUser();
        System.out.println("[SHOW USER PROFILE]");
        System.out.println(currentUser.getLogin() + ", " + currentUser.getRole());
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
